WHERE=`/usr/bin/uname -n`

CPPFLAGS += -g --std=c++14 -Wunused-value
INCLUDE_DIR +=-I/usr/local/include -I/usr/include -I.
LIBS_DIR +=-L/usr/lib -L. -L/usr/local/lib

.cpp.o:

	clang++ -o $@ -c $< $(INCLUDE_DIR) $(CPPFLAGS)

all: whitenoise.o

whitenoise.o: whitenoise.cpp whitenoise.hpp


clean:

#	echo ${.TARGETS}
	@echo -n Cleaning ...
	@touch a.o a.core 
	@- rm *.o
	@- rm *core
	@echo Done.


git: clean

	git add -A .
	git commit -m "$(CM)"
	git push



#ifndef	WHITE_NOISE 
#define WHITE_NOISE

#include <vector>

class WhiteNoise {

#define	MAGIC_NUMBER	"NOISE V1.0"
    
public:
	// Simple Constructor
   WhiteNoise();				
	// mainly Constructor
   WhiteNoise(double m, double v, long s, bool snr=false);  
   WhiteNoise(double snr, long values); 
	// Destructor to clear the vector
	~WhiteNoise();		
   

	void						showValues(bool newLine=false);
	bool						saveValues(std::string fn);
	void						setVariance(double v){ this->var = v; }
	void						setNumberOfValues(unsigned long int n){ this->num=n; }
	void						setSNR(bool snr){ this->withsnr = snr; }
	void						setMean(double m){ this->mean=m; }
	void						setSigma(double s){ this->sigma = s; }
	double					getVariance(void){ return this->var; }
	double					getMean(void){ return this->mean; }
	unsigned long int 	getNumberOfValues(void){ return this->num; }
	bool 						getSNR(void){ return this->withsnr; }
	double					getNoise(unsigned long p){return ((p < this->noiseVector.size())?this->noiseVector[p]:-0.0000000001); }

   std::vector<double> 	generateNoiseValues();
   std::vector<double> 	generateNoiseValues(unsigned long int numberofvalues);

    
private:
   double 				mean;
   double 				var;
   unsigned long 		num;
   double 				sigma;
   bool 				withsnr;
   std::vector<double> 	noiseVector; 
};

#endif

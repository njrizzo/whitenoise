# WhiteNoise

This a simple White Noise Genarator.


```
                        // Simple Constructor
                        WhiteNoise();
                        // mainly Constructor
                        WhiteNoise(double m, double v, long s, bool snr=false);
                        WhiteNoise(double snr, long values); 
                        ~WhiteNoise();
   std::vector<double>  generateNoiseValues();
   void                 showValues(bool newLine=false);
   bool                 saveValues(std::string fn);

```    
